<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/upload', function (Request $request) {
    if ($request->hasFile('files')) {
        $request->validate([
            'files.*' => 'required|mimes:pdf,doc,docx|max:2048',
        ]);

        $uploadedFiles = [];

        foreach ($request->file('files') as $file) {
            $fileExtension = $file->getClientOriginalExtension();
            $uploadPath = 'uploads/' . uniqid() . '.' . $fileExtension;

            $file->move(public_path('uploads'), $uploadPath);

            $uploadedFiles[] = [
                'fileType' => $fileExtension,
                'filePath' => asset($uploadPath),
            ];
        }

        return response()->json($uploadedFiles);
    } else {
        return response('No files uploaded', 400);
    }
});

Route::get('/package', function () {
    return view('package');
});
