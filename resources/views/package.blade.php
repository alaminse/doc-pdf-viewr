<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel Exam</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .preview {
            margin-top: 20px;
        }

        .preview canvas {
            width: 100px;
            height: 100px;
        }

        .preview canvas:hover {
            border: 2px solid #007bff;
        }

        #textContainer {
            margin-top: 20px;
        }
    </style>
</head>
<body class="antialiased">
    <div class="container mt-5">
        <div class="row">
            <div class="col-4">
                <form class="row g-3" id="fileUploadForm">
                    <div class="col-auto">
                        <label for="file" class="visually-hidden">File</label>
                        <input type="file" class="form-control" id="file" multiple>
                    </div>
                    <div class="col-auto">
                        <button type="button" class="btn btn-primary mb-3" onclick="uploadFile()">Confirm</button>
                    </div>
                </form>
                <div class="preview" id="previewContainer"></div>
                <div id="textContainer"></div>
            </div>
            <div class="col-8">
                <div id="div-iframe"></div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>

</body>
</html>
