<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel Exam</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        .preview {
            margin-top: 20px;
        }

        .preview canvas {
            width: 100px;
            height: 100px;
        }

        .preview canvas:hover {
            border: 2px solid #007bff;
        }

        #textContainer {
            margin-top: 20px;
        }
    </style>
</head>
<body class="antialiased">
    <div class="container mt-5">
        <div class="row">
            <div class="col-4">
                <form class="row g-3" id="fileUploadForm">
                    <div class="col-auto">
                        <label for="file" class="visually-hidden">File</label>
                        <input type="file" class="form-control" id="file" multiple>
                    </div>
                    <div class="col-auto">
                        <button type="button" class="btn btn-primary mb-3" onclick="uploadFile()">Confirm</button>
                    </div>
                </form>
                <div class="preview" id="previewContainer"></div>
                <div id="textContainer"></div>
            </div>
            <div class="col-8">
                <div id="div-iframe"></div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.9.359/pdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.17.1/xlsx.full.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/jszip/dist/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mammoth/1.0.1/mammoth.browser.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/viewerjs@1.7.0/dist/viewer.css">
    <script type="text/javascript" src="https://unpkg.com/viewerjs@1.7.0/dist/viewer.js"></script>

    <script>
        function uploadFile() {
            var files = $('#file')[0].files;

            if (files.length > 0) {
                var formData = new FormData();
                for (var i = 0; i < files.length; i++) {
                    formData.append('files[]', files[i]);
                }

                $.ajax({
                    url: '/upload',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response) {
                        $('#previewContainer').html('');
                        $('#div-iframe').html('');
                        $('#textContainer').html('');

                        // Loop through each uploaded file
                        for (var i = 0; i < response.length; i++) {
                            var fileType = response[i].fileType;
                            var filePath = response[i].filePath;

                            if (fileType === 'pdf') {
                                // Embed PDF using pdf.js (as you did before)
                                var pdfPreview = $('<canvas class="pdf-preview" style="width:100px; height:100px;"></canvas>');
                                $('.preview').append(pdfPreview);

                                pdfPreview.hover(
                                    function () {
                                        $(this).css('border', '2px solid #007bff');
                                    },
                                    function () {
                                        $(this).css('border', 'none');
                                    }
                                );

                                pdfPreview.on('click', function () {
                                    showLargePDFPreview(filePath);
                                });

                                displayPDF(filePath, pdfPreview.get(0));
                            } else if (fileType === 'xlsx') {
                                // Handle XLSX files using SheetJS
                                var xlsxPreview = $('<div class="xlsx-preview"></div>');
                                $('.preview').append(xlsxPreview);

                                // Read the XLSX file and render the preview
                                readXLSXFile(filePath, xlsxPreview);
                            } else if (fileType === 'docx') {

                                $('.preview').append(
                                    '<div onclick="initializeViewer(\'' + filePath + '\')" style="height: 100px; width: 100px; background-color: black; color: white; cursor: pointer;">DOCX</div>'
                                );
                            } else {

                            }
                        }
                    },

                    error: function (error) {
                        console.error('Error uploading file:', error);
                    }
                });
            } else {
                alert('Please select a file.');
            }
        }

        function previewWordDoc(filePath) {
            // initializeViewer(filePath);
            const docxFileLink = filePath;
            const docxContentDiv = document.getElementById('textContainer');
            docxContentDiv.innerHTML = '';
            fetch(docxFileLink)
            .then(response => response.arrayBuffer())
            .then(arrayBuffer => {
                const uint8Array = new Uint8Array(arrayBuffer);

                mammoth.extractRawText({ arrayBuffer: uint8Array })
                .then(result => {
                    docxContentDiv.innerHTML = result.value;
                })
                .catch(error => {
                    console.error('Error extracting DOCX content:', error);
                });
            })
            .catch(error => {
                console.error('Error fetching DOCX file:', error);
            });
        }

        function initializeViewer(filePath) {
            previewWordDoc(filePath)
            const docxFileLink = filePath;

            // Create an iframe element
            const iframe = document.createElement('iframe');
            iframe.className = 'word';
            iframe.src = `https://docs.google.com/gview?url=${docxFileLink}&embedded=true`;

            // Append the iframe to the container
            const container = document.getElementById('div-iframe');
            container.innerHTML = ''; // Clear previous content
            container.appendChild(iframe);
        }




        function displayPDF(pdfPath, canvas) {
            var loadingTask = pdfjsLib.getDocument(pdfPath);
            loadingTask.promise.then(function (pdfDocument) {
                // Fetch the first page
                pdfDocument.getPage(1).then(function (pdfPage) {
                    var viewport = pdfPage.getViewport({ scale: 1.0 });
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;

                    // Render PDF page into canvas context
                    var renderContext = {
                        canvasContext: context,
                        viewport: viewport
                    };
                    pdfPage.render(renderContext);
                });
            });
        }

        function showLargePDFPreview(pdfPath) {
            $('#div-iframe').html('<iframe src="about:blank" style="width:100%; height:1000px; border: 0;"></iframe>');

            var iframe = $('#div-iframe').find('iframe')[0];
            var iframeDocument = iframe.contentDocument || iframe.contentWindow.document;

            var embedContainer = iframeDocument.createElement('div');
            embedContainer.style.width = '100%';
            embedContainer.style.height = '100%';

            iframeDocument.body.appendChild(embedContainer);

            var embedCanvas = iframeDocument.createElement('canvas');
            embedCanvas.style.width = '100%';
            embedCanvas.style.height = '100%';
            embedCanvas.style.border = '0'; // Set border to 0 to avoid any potential styling conflicts
            embedContainer.appendChild(embedCanvas);

            // Display PDF on canvas
            displayPDF(pdfPath, embedCanvas);

            // Extract text content from PDF
            extractTextFromPDF(pdfPath);
        }

        function extractTextFromPDF(pdfPath) {
            var loadingTask = pdfjsLib.getDocument(pdfPath);

            loadingTask.promise.then(function (pdfDocument) {
                pdfDocument.getPage(1).then(function (pdfPage) {
                    pdfPage.getTextContent().then(function (textContent) {
                        var textDiv = $('#textContainer');
                        textDiv.html('');

                        textContent.items.forEach(function (textItem) {
                            var textSpan = document.createElement('span');
                            textSpan.textContent = textItem.str;
                            textDiv.append(textSpan);
                        });
                    });
                });
            });
        }
    </script>
</body>
</html>
